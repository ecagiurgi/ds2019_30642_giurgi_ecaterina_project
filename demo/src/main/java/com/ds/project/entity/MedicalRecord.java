package com.ds.project.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "medicationRecord")
public class MedicalRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long medicalRecordId;
    private String description;

    @OneToMany
    @JoinColumn(name = "medicalRecordId")
    private List<Patient> patients = new ArrayList<>();

    public MedicalRecord(String description) {
        this.description = description;
    }

    public MedicalRecord(){}

    public Long getMedicalRecordId() {
        return medicalRecordId;
    }

    public void setMedicalRecordId(Long medicalRecordId) {
        this.medicalRecordId = medicalRecordId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
