package com.ds.project.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "patient")
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = "user_id", name = "fk_user1")}
)
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long patientId;
    private String name;
    private LocalDate birthDate;
    private String adress;

    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="caregiverId")
    private Caregiver caregiver;

    public Patient(String name, LocalDate birthDate, String adress, Gender gender) {
        this.name = name;
        this.birthDate = birthDate;
        this.adress = adress;
        this.gender = gender;
    }

    public Patient() {
    }

    public Long getId() {
        return patientId;
    }

    public void setId(Long id) {
        this.patientId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }
}
