package com.ds.project.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "doctor")
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = "user_id", name = "fk_user")}
)
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long doctorId;
    private String name;
    private LocalDate birthDate;
    private String adress;

    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "user_id")
    private User user1;


    public Doctor(String name, LocalDate birthDate, String adress, Gender gender) {
        this.name = name;
        this.birthDate = birthDate;
        this.adress = adress;
        this.gender = gender;
    }

    public Doctor() {
    }

    public Long getId() {
        return doctorId;
    }

    public void setId(Long id) {
        this.doctorId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }
}
