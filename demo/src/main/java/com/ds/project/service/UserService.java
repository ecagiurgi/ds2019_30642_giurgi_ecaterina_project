package com.ds.project.service;



import com.ds.project.dto.LoginDTO;
import com.ds.project.dto.UserDTO;
import org.hibernate.type.UUIDBinaryType;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public interface UserService {
    UserDTO save(UserDTO userDto);

    String test();

    UserDTO deleteUser(UUID id);

    List<UserDTO> findAll();

    UserDTO findByUserId(UUID id);

    UserDTO login(LoginDTO loginDTO);

    UserDTO register(UserDTO userDto);

    UserDTO changePassword(@NotNull String username, @NotNull String password, @NotNull String newPassword);


}
