package com.ds.project.service;

import com.ds.project.dto.SideEffectDTO;

import java.util.List;

public interface SideEffectService {
    SideEffectDTO save(SideEffectDTO sideEffectDTO);

    List<SideEffectDTO> findAll();

    SideEffectDTO findById(Long id);

    SideEffectDTO deleteSideEffect(Long id);

}
