package com.ds.project.service;

import com.ds.project.dto.CaregiverDTO;

import java.util.List;

public interface CaregiverService {
    CaregiverDTO save(CaregiverDTO caregiverDTO);

    List<CaregiverDTO> findAll();

    CaregiverDTO findById(Long id);

    CaregiverDTO deleteCaregiver(Long id);

}
