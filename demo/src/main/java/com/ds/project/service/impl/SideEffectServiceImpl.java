package com.ds.project.service.impl;

import com.ds.project.dto.SideEffectDTO;
import com.ds.project.entity.SideEffect;
import com.ds.project.mapper.SideEffectMapper;
import com.ds.project.repository.SideEffectsRepository;
import com.ds.project.service.SideEffectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SideEffectServiceImpl implements SideEffectService {


    private final SideEffectMapper sideEffectMapper;
    private final SideEffectsRepository sideEffectsRepository;


    @Autowired
    public SideEffectServiceImpl(SideEffectMapper sideEffectMapper,
                              SideEffectsRepository sideEffectsRepository) {
        this.sideEffectMapper = sideEffectMapper;
        this.sideEffectsRepository = sideEffectsRepository;
    }
    @Override
    public SideEffectDTO save(SideEffectDTO sideEffectDTO) {
        SideEffect sideEffect;
        if (sideEffectDTO.getSideEffectId() != null) {
            sideEffect = sideEffectsRepository.findBySideEffectId(sideEffectDTO.getSideEffectId());
        } else {
            sideEffect = new SideEffect();
        }
        sideEffectMapper.toEntityUpdate(sideEffect, sideEffectDTO);
        SideEffectDTO sideEffectDTO1 = sideEffectMapper.toDTO(sideEffectsRepository.save(sideEffect));
        return sideEffectDTO1;       }

    @Override
    public List<SideEffectDTO> findAll() {
        return sideEffectsRepository.findAll()
                .stream()
                .map(sideEffectMapper::toDTO)
                .collect(Collectors.toList());        }

    @Override
    public SideEffectDTO findById(Long id) {
        return sideEffectMapper.toDTO(sideEffectsRepository.findBySideEffectId(id));
    }

    @Override
    public SideEffectDTO deleteSideEffect(Long id) {
        sideEffectsRepository.deleteById(id);
        return null;
    }
}
