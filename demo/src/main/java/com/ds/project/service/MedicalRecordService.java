package com.ds.project.service;

import com.ds.project.dto.MedicalRecordDTO;

import java.util.List;

public interface MedicalRecordService {
    MedicalRecordDTO save(MedicalRecordDTO medicalRecordDTO);

    List<MedicalRecordDTO> findAll();

    MedicalRecordDTO findById(Long id);

    MedicalRecordDTO deleteMedicalRecord(Long id);

}
