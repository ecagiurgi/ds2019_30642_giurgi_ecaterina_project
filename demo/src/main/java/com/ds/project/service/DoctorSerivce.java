package com.ds.project.service;


import com.ds.project.dto.DoctorDTO;

import java.util.List;

public interface DoctorSerivce {
    DoctorDTO save(DoctorDTO doctorDto);

    List<DoctorDTO> findAll();

    DoctorDTO findById(Long id);

    void deleteDoctor(Long id);


}
