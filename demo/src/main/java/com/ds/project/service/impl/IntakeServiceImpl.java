package com.ds.project.service.impl;

import com.ds.project.dto.IntakeDTO;
import com.ds.project.entity.Intake;
import com.ds.project.mapper.IntakeMapper;
import com.ds.project.repository.IntakeRepository;
import com.ds.project.service.IntakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class IntakeServiceImpl implements IntakeService {

    private final IntakeMapper intakeMapper;
    private final IntakeRepository intakeRepository;


    @Autowired
    public IntakeServiceImpl( IntakeMapper intakeMapper,
                              IntakeRepository intakeRepository) {
        this.intakeMapper = intakeMapper;
        this.intakeRepository = intakeRepository;
    }


    @Override
    public IntakeDTO save(IntakeDTO intakeDTO) {
        Intake intake;
        if (intakeDTO.getIntakeId() != null) {
            intake = intakeRepository.findByIntakeId(intakeDTO.getIntakeId());
        } else {
            intake = new Intake();
        }
        intakeMapper.toEntityUpdate(intake, intakeDTO);
        IntakeDTO intakeDTO1 = intakeMapper.toDTO(intakeRepository.save(intake));
        return intakeDTO1;    }

    @Override
    public List<IntakeDTO> findAll() {
        return intakeRepository.findAll()
                .stream()
                .map(intakeMapper::toDTO)
                .collect(Collectors.toList());     }

    @Override
    public IntakeDTO findById(Long id) {
        return intakeMapper.toDTO(intakeRepository.findByIntakeId(id));
    }

    @Override
    public IntakeDTO deleteIntake(Long id) {
        intakeRepository.deleteById(id);
        return null;    }
}
