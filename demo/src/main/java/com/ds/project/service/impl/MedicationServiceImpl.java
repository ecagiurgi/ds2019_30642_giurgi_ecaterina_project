package com.ds.project.service.impl;

import com.ds.project.dto.MedicationDTO;
import com.ds.project.entity.Medication;
import com.ds.project.mapper.MedicationMapper;
import com.ds.project.repository.MedicationRepository;
import com.ds.project.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicationServiceImpl implements MedicationService {

    private final MedicationMapper medicationMapper;
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationServiceImpl(MedicationMapper medicationMapper,
                                 MedicationRepository medicationRepository) {
        this.medicationMapper = medicationMapper;
        this.medicationRepository = medicationRepository;
    }

    @Override
    public MedicationDTO save(MedicationDTO medicationDTO) {
        Medication medication;
        if (medicationDTO.getId() != null) {
            medication = medicationRepository.findByMedicationId(medicationDTO.getId());
        } else {
            medication = new Medication();
        }
        medicationMapper.toEntityUpdate(medication, medicationDTO);
        MedicationDTO medicationDTO1 = medicationMapper.toDTO(medicationRepository.save(medication));
        return medicationDTO1;
    }

    @Override
    public List<MedicationDTO> findAll() {
        return medicationRepository.findAll()
                .stream()
                .map(medicationMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public MedicationDTO findById(Long id) {
        return medicationMapper.toDTO(medicationRepository.findByMedicationId(id));
    }

    @Override
    public MedicationDTO deleteMedication(Long id) {
        medicationRepository.deleteById(id);
        return null;
    }
}
