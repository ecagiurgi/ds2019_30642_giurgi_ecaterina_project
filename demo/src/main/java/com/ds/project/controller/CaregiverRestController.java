package com.ds.project.controller;

import com.ds.project.dto.CaregiverDTO;
import com.ds.project.service.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "http://172.25.0.4:4200")
@RestController
@RequestMapping("/caregiver")
public class CaregiverRestController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverRestController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping("/list")
    public List<CaregiverDTO> list() {
        return caregiverService.findAll();
    }

    @GetMapping("/{id}")
    public CaregiverDTO findById(@PathVariable("id") Long id) {
        return caregiverService.findById(id);
    }

    @PostMapping("/save")
    public CaregiverDTO save(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.save(caregiverDTO);
    }

    @GetMapping("/delete/{id}")
    public CaregiverDTO delete(@PathVariable("id") Long id) {
        return caregiverService.deleteCaregiver(id);
    }


}
