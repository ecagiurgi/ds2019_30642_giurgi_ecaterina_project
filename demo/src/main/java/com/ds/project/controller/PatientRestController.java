package com.ds.project.controller;

import com.ds.project.dto.PatientDTO;
import com.ds.project.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://172.25.0.4:4200")
@RestController
@RequestMapping("/patient")
public class PatientRestController {
    private final PatientService patientService;

    @Autowired
    public PatientRestController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping("/list")
    public List<PatientDTO> list() {
        return patientService.findAll();
    }

    @GetMapping("/{id}")
    public PatientDTO findById(@PathVariable("id") Long id) {
        return patientService.findById(id);
    }

    @PostMapping("/save")
    public PatientDTO save(@RequestBody PatientDTO patientDTO) {
        return patientService.save(patientDTO);
    }

    @GetMapping("/delete/{id}")
    public PatientDTO delete(@PathVariable("id") Long id) {
        return patientService.deletePatient(id);
    }


}
