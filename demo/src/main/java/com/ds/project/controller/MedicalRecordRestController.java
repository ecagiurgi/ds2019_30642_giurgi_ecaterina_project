package com.ds.project.controller;

import com.ds.project.dto.MedicalRecordDTO;
import com.ds.project.service.MedicalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "http://172.25.0.4:4200")
@RestController
@RequestMapping("/medicalRecord")
public class MedicalRecordRestController {
    private final MedicalRecordService medicalRecordService;

    @Autowired
    public MedicalRecordRestController(MedicalRecordService medicalRecordService) {
        this.medicalRecordService = medicalRecordService;
    }

    @GetMapping("/list")
    public List<MedicalRecordDTO> list() {
        return medicalRecordService.findAll();
    }

    @GetMapping("/{id}")
    public MedicalRecordDTO findById(@PathVariable("id") Long id) {
        return medicalRecordService.findById(id);
    }

    @PostMapping("/save")
    public MedicalRecordDTO save(@RequestBody MedicalRecordDTO medicalRecordDTO) {
        return medicalRecordService.save(medicalRecordDTO);
    }

    @GetMapping("/delete/{id}")
    public MedicalRecordDTO delete(@PathVariable("id") Long id) {
        return medicalRecordService.deleteMedicalRecord(id);
    }


}
