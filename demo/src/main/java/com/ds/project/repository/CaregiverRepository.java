package com.ds.project.repository;

import com.ds.project.entity.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaregiverRepository  extends JpaRepository<Caregiver, Long> {
    Caregiver findByCaregiverId(final Long id);

}
