package com.ds.project.repository;

import com.ds.project.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository  extends JpaRepository<Patient, Long> {
    Patient findByPatientId(final Long id);
}
