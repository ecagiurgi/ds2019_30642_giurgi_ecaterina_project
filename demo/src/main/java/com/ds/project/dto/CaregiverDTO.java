package com.ds.project.dto;


import com.ds.project.entity.Gender;

import java.time.LocalDate;

public class CaregiverDTO {

    private Long id;
    private String name;
    private LocalDate birthDate;
    private String adress;
    private Gender gender;
    private UserDTO user2DTO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public UserDTO getUser2DTO() {
        return user2DTO;
    }

    public void setUser2DTO(UserDTO user2DTO) {
        this.user2DTO = user2DTO;
    }
}
