package com.ds.project.mapper.impl;

import com.ds.project.dto.PatientDTO;
import com.ds.project.entity.Patient;
import com.ds.project.mapper.CaregiverMapper;
import com.ds.project.mapper.PatientMapper;
import com.ds.project.mapper.UserMapper;
import org.springframework.stereotype.Component;

@Component
public class PatientMapperImpl implements PatientMapper {
    UserMapper userMapper = new UserMapperImpl();
    CaregiverMapper caregiverMapper = new CaregiverMapperImpl();

    @Override
    public PatientDTO toDTO(Patient entity) {
        if (entity == null) {
            return null;
        }

        PatientDTO patientDTO = new PatientDTO();

        patientDTO.setId(entity.getId());
        patientDTO.setName(entity.getName());
        patientDTO.setBirthDate(entity.getBirthDate());
        patientDTO.setAdress(entity.getAdress());
        patientDTO.setGender(entity.getGender());
        patientDTO.setUserDTO(userMapper.toDTO(entity.getUser()));
        patientDTO.setCaregiverDTO(caregiverMapper.toDTO(entity.getCaregiver()));

        return patientDTO;
    }

    @Override
    public Patient toEntity(PatientDTO dto) {
        if (dto == null) {
            return null;
        }

        Patient patient = new Patient();

        patient.setId(dto.getId());
        patient.setName(dto.getName());
        patient.setBirthDate(dto.getBirthDate());
        patient.setAdress(dto.getAdress());
        patient.setGender(dto.getGender());
        patient.setUser(userMapper.toEntity(dto.getUserDTO()));
        patient.setCaregiver(caregiverMapper.toEntity(dto.getCaregiverDTO()));

        return patient;
    }

    @Override
    public void toEntityUpdate(Patient patient, PatientDTO patientDTO) {
        if (patientDTO == null) {
            return;
        }
        patient.setId(patientDTO.getId());
        patient.setName(patientDTO.getName());
        patient.setBirthDate(patientDTO.getBirthDate());
        patient.setAdress(patientDTO.getAdress());
        patient.setGender(patientDTO.getGender());
        patient.setUser(userMapper.toEntity(patientDTO.getUserDTO()));
        patient.setCaregiver(caregiverMapper.toEntity(patientDTO.getCaregiverDTO()));

    }
}
