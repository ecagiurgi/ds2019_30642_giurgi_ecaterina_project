package com.ds.project.mapper;

import com.ds.project.dto.MedicationDTO;
import com.ds.project.entity.Medication;

public interface MedicationMapper {
    MedicationDTO toDTO(Medication entity);
    Medication toEntity(MedicationDTO dto);
    void toEntityUpdate(Medication medication, MedicationDTO medicationDTO);
}
