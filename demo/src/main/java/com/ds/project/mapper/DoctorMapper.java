package com.ds.project.mapper;

import com.ds.project.dto.DoctorDTO;
import com.ds.project.dto.UserDTO;
import com.ds.project.entity.Doctor;
import com.ds.project.entity.User;

public interface DoctorMapper {

    DoctorDTO toDTO(Doctor entity);
    Doctor toEntity(DoctorDTO dto);
    void toEntityUpdate(Doctor doctor, DoctorDTO doctorDTO);
}
