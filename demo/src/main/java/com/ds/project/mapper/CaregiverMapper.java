package com.ds.project.mapper;

import com.ds.project.dto.CaregiverDTO;
import com.ds.project.entity.Caregiver;

public interface CaregiverMapper {

    CaregiverDTO toDTO(Caregiver entity);
    Caregiver toEntity(CaregiverDTO dto);
    void toEntityUpdate(Caregiver caregiver, CaregiverDTO caregiverDTO);
}
