package com.ds.project.mapper.impl;

import com.ds.project.dto.UserDTO;
import com.ds.project.entity.User;
import com.ds.project.mapper.UserMapper;
import org.springframework.stereotype.Component;

@Component
public class UserMapperImpl implements UserMapper {
    @Override
    public UserDTO toDTO(User entity) {
        if ( entity == null ) {
            return null;
        }

        UserDTO userDTO = new UserDTO();

        userDTO.setPassword( entity.getPassword() );
        userDTO.setType( entity.getType() );
        userDTO.setUserId( entity.getUserId() );
        userDTO.setUsername( entity.getUsername() );

        return userDTO;    }

    @Override
    public User toEntity(UserDTO dto) {
        if ( dto == null ) {
            return null;
        }

        User user = new User();

        user.setPassword( dto.getPassword() );
        user.setType( dto.getType() );
        user.setUserId( dto.getUserId() );
        user.setUsername( dto.getUsername() );

        return user;    }

    @Override
    public void toEntityUpdate(User user, UserDTO userDTO) {
        if ( userDTO == null ) {
            return;
        }

        user.setUsername( userDTO.getUsername() );
        user.setPassword( userDTO.getPassword() );
        user.setType( userDTO.getType() );
        user.setUserId( userDTO.getUserId() );
    }
}
