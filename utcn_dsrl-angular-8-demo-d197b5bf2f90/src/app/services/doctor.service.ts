import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {Observable, throwError} from "rxjs";
import {UserView} from "../model/UserView";
import {REST_API} from "../common/API";
import {catchError} from "rxjs/operators";
import {ErrorService} from "../utils/error-service";
import {User} from "../model/User";
import {DoctorView} from "../model/DoctorView";
import {Doctor} from "../model/Doctor";

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  constructor(private http: HttpClient, private dialog: MatDialog) {
  }

  getDoctors(): Observable<DoctorView[]> {
    return this.http.get<DoctorView[]>(REST_API + 'doctor/list')
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }

  insertDoctor(doctor: Doctor): Observable<Doctor> {
    return this.http.post<any>(REST_API + 'doctor/save', doctor)
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }



  getUserById(id: number): Observable<DoctorView> {
    return this.http.get<DoctorView>(REST_API + 'doctor/' + id)
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }

  deleteDoctorById(id: number) {
    return this.http.get<DoctorView>(REST_API + 'doctor/delete/' + id);
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }
}
