import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {Observable, throwError} from "rxjs";
import {UserView} from "../model/UserView";
import {REST_API} from "../common/API";
import {catchError} from "rxjs/operators";
import {ErrorService} from "../utils/error-service";
import {Intake} from "../model/Intake";

@Injectable({
  providedIn: 'root'
})
export class IntakeService {

  constructor(private http: HttpClient, private dialog: MatDialog) {
  }

  getIntakes(): Observable<Intake[]> {
    return this.http.get<Intake[]>(REST_API + 'intake/list')
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }
}
