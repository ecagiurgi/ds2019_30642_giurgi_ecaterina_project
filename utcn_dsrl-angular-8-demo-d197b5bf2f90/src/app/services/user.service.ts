import {Inject,Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserView} from '../model/UserView';
import {User} from '../model/User';
import {REST_API} from '../common/API';
import {Observable, throwError} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {ErrorService} from '../utils/error-service';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import {catchError} from 'rxjs/operators';
import {Login} from "../model/Login";
import {SESSION_STORAGE, WebStorageService} from "angular-webstorage-service";


@Injectable({
  providedIn: 'root'
})
export class UserService {
  currentUser: UserView;

  constructor(private http: HttpClient, private dialog: MatDialog) {
  }

  login(login: Login): Observable<UserView> {
    let reqHeader = new HttpHeaders({'Content-Type': 'application/json'});
    console.log(login);
    return this.http.post<UserView>(REST_API + 'user/login', login, {headers: reqHeader});
  }

  logout() {
    let reqHeader = new HttpHeaders({'Content-Type': 'application/json'});
    if (localStorage.getItem('user') != null) {
      localStorage.removeItem('user');
    }
  }

  getUsers(): Observable<UserView[]> {
    return this.http.get<UserView[]>(REST_API + 'user/list')
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }

  insertUser(user: User): Observable<User> {
    return this.http.post<any>(REST_API + 'user/save', user)
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }

  // getUserById(id: string): Observable<UserView> {
  //   return this.http.get<UserView>(REST_API + 'user/' + id)
  //   catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  // }
  getUserById(id: string): Observable<User> {
    return this.http.get<User>(REST_API + 'user/' + id)
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }
}


