import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {Observable, throwError} from "rxjs";
import {REST_API} from "../common/API";
import {catchError} from "rxjs/operators";
import {ErrorService} from "../utils/error-service";
import {PatientView} from "../model/PatientView";
import {Patient} from "../model/Patient";

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private http: HttpClient, private dialog: MatDialog) {
  }

  getPatients(): Observable<PatientView[]> {
    return this.http.get<PatientView[]>(REST_API + 'patient/list')
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }

  insertPatient(patient: Patient): Observable<Patient> {
    return this.http.post<any>(REST_API + 'patient/save', patient)
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }
  //
  // getUserById(id: number): Observable<UserView> {
  //   return this.http.get<UserView>(REST_API + 'user/' + id)
  //   catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  // }
}
