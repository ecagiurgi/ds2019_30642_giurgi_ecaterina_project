import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {Observable, throwError} from "rxjs";
import {REST_API} from "../common/API";
import {catchError} from "rxjs/operators";
import {ErrorService} from "../utils/error-service";
import {CaregiverView} from "../model/CaregiverView";
import {Caregiver} from "../model/Caregiver";

@Injectable({
  providedIn: 'root'
})
export class CaregiverService {

  constructor(private http: HttpClient, private dialog: MatDialog) {
  }

  getCaregivers(): Observable<CaregiverView[]> {
    return this.http.get<CaregiverView[]>(REST_API + 'caregiver/list')
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }

  insertCaregiver(caregiver: Caregiver): Observable<Caregiver> {
    return this.http.post<any>(REST_API + 'caregiver/save', caregiver)
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }
  //
  // getUserById(id: number): Observable<UserView> {
  //   return this.http.get<UserView>(REST_API + 'user/' + id)
  //   catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  // }

  deleteCaregiverById(id: number) {
    return this.http.get<CaregiverView>(REST_API + 'caregiver/delete/' + id);
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }

  getActivities(): Observable<string[]> {
    return this.http.get<string[]>(REST_API + 'caregiver/activities')
    catchError((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }

}
