import {Component, Inject, OnInit} from '@angular/core';
import {Login} from "../model/Login";
import {UserService} from "../services/user.service";
import {UserView} from "../model/UserView";
import {Router} from "@angular/router";
import {SESSION_STORAGE, WebStorageService} from "angular-webstorage-service";
import {HeaderComponent} from "../header/header.component";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login: Login;
  currentUser: UserView;
  constructor(private userService: UserService, private router: Router) {
    this.login = {
      username: '',
      password: ''
    }

  }

  ngOnInit() {
    // HeaderComponent.arguments.isButtonVisible = false;
  }

  onSubmit() {
    this.userService.login(this.login).subscribe((response: any) => {
      const user = (<UserView>response);
      if (user) {
        this.currentUser = user // Setting up user data in userData var
        localStorage.setItem('user', JSON.stringify(this.currentUser));
        JSON.parse(localStorage.getItem('user'));
        console.log(localStorage.getItem('user'))
        if (user.type === "DOCTOR"){
        //  HeaderComponent.caller.arguments.isButtonVisible = true;
          this.router.navigateByUrl('doctor');
        }
        else if (user.type === "PATIENT")
          this.router.navigateByUrl('patient');
        else if (user.type === "CAREGIVER")
          this.router.navigateByUrl('patient');
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
        console.log(localStorage.getItem('user'))

      }
    })
  }

}
