import {Component, OnInit} from '@angular/core';
import {DoctorService} from "../services/doctor.service";
import {Doctor} from "../model/Doctor";
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-doctors',
  templateUrl: './create-doctor.component.html',
  styleUrls: ['./create-doctor.component.css']
})
export class CreateDoctorComponent implements OnInit {

  doctor: Doctor;

  constructor(private doctorService: DoctorService, private userService: UserService) {
    this.doctor = {
      name: '',
      adress: '',
      birthDate: '',
      gender: '',
      user1Dto: {
        username: '',
        password: '',
        type: "DOCTOR"
      }
    }
  }


  ngOnInit() {
  }

  onSubmit() {
    console.log(this.doctor);
    this.doctorService.insertDoctor(this.doctor).subscribe(value => {
      console.log(value);
      alert(this.doctor.name + ' successfull inserted');
    })
  }

}
