import {BrowserModule} from '@angular/platform-browser';

import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core'
import {AppComponent} from './app.component';
import {UsersComponent} from './users/users.component';
import {HomeComponent} from './home/home.component';
import {HeaderComponent} from './header/header.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ErrorHandlingComponent } from './error-handling/error-handling.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { MatIconModule } from "@angular/material/icon";
import { MatMenuModule } from "@angular/material/menu";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatTableModule } from "@angular/material/table";
import { MatToolbarModule } from "@angular/material/toolbar";
import {HttpClientModule} from "@angular/common/http";
import { LoginComponent } from './login/login.component';
import {DoctorsComponent} from "./doctors/doctors.component";
import { CaregiversComponent } from './caregivers/caregivers.component';
import { PatientsComponent } from './patients/patients.component';
import { CreateDoctorComponent } from './create-doctor/create-doctor.component';
import {CreateCaregiverComponent} from "./create-caregiver/create-caregiver.component";
import { StorageServiceModule} from 'angular-webstorage-service';
import { CreatePatientComponent } from './create-patient/create-patient.component';
import { UserActivityComponent } from './user-activity/user-activity.component'
import {IntakesComponent} from "./intake/intakes.component";
const appRoutes: Routes = [
  {path:'', component:HomeComponent},
  {path:'user/login', component:LoginComponent},
  {path:'user', component:UsersComponent},
  {path:'doctor', component:DoctorsComponent},
  {path:'caregiver', component:CaregiversComponent},
  {path:'patient', component:PatientsComponent},
  {path:'doctor/save', component:CreateDoctorComponent},
  {path:'caregiver/save', component:CreateCaregiverComponent},
  {path:'patient/save', component:CreatePatientComponent},
  {path:'activity', component:UserActivityComponent},
  {path:'intake', component:IntakesComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    HomeComponent,
    HeaderComponent,
    ErrorHandlingComponent,
    LoginComponent,
    DoctorsComponent,
    CaregiversComponent,
    PatientsComponent,
    CreateDoctorComponent,
    CreateCaregiverComponent,
    CreatePatientComponent,
    UserActivityComponent,
    IntakesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    HttpClientModule,
    MatPaginatorModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    StorageServiceModule
  ],
  entryComponents: [
    ErrorHandlingComponent
  ],
  exports: [
    MatPaginatorModule,
    MatTableModule,
    ErrorHandlingComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
