import {User} from "./User";

export class CaregiverView {
  id: number;
  name: string;
  birthDate: string;
  adress: string;
  gender: string;
  user2DTO: User;
}
