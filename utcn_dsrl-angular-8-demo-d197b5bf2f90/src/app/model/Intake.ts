import {Timestamp} from "rxjs";
import {PatientView} from "./PatientView";
import {Medication} from "./Medication";


export class Intake {
  intakeId: number;
  startDate: Timestamp<any>;
  endDate: Timestamp<any>;
  dosage: number;
  medicationDTO: Medication;
  patientDTO: PatientView;
  status: string;
}
