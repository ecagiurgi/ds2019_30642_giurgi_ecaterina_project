import {User} from "./User";

export class DoctorView {
  doctorId: number;
  name: string;
  birthDate: string;
  adress: string;
  gender: string;
  user1Dto: User;
}
