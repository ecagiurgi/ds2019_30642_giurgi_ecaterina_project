import {User} from "./User";

export class Doctor {
  name: string;
  birthDate: string;
  adress: string;
  gender: string;
  user1Dto: User;
}
