import {SideEffect} from "./SideEffect";

export class Medication {
  id:number;
  name: string;
  dosage: number;
  sideEffectDTOS: SideEffect[];
}
