import {User} from "./User";

export class Caregiver {
  name: string;
  birthDate: string;
  adress: string;
  gender: string;
  user2DTO: User;
}
