import {User} from "./User";
import {Caregiver} from "./Caregiver";
import {CaregiverView} from "./CaregiverView";

export class Patient {
  name: string;
  birthDate: string;
  adress: string;
  gender: string;
  userDTO: User;
  caregiverDTO:CaregiverView;
}
