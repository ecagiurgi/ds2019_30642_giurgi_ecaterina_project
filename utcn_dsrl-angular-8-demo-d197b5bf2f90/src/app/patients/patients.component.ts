import {Component, OnInit} from '@angular/core';
import {User} from '../model/User';
import {DoctorService} from '../services/doctor.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PatientView} from "../model/PatientView";
import {Patient} from "../model/Patient";
import {PatientService} from "../services/patient.service";

@Component({
  selector: 'app-users',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {

  patients: PatientView[] = [];
  patientInsert: Patient;
  displayedColumns: string[] = ['name', 'birthDate', 'adress','gender','caregiver'];
  registerForm: FormGroup;
  submitted: boolean;

  constructor(private router: Router, private patientService: PatientService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.submitted = false;
    this.updatePatientTable();
  }

  updatePatientTable() {
    this.registerForm = this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      birthdate: ['', [Validators.required]],
      adress: ['', Validators.required],
      gender: ['', Validators.required],
      caregiverDTO: ['', Validators.required]
    });
    this.patientInsert = new Patient();
    this.patientService.getPatients()
      .subscribe(data => {
        console.log(data);
        this.patients = data;
      });
  }

  get requestedForm() { return this.registerForm.controls; }

  // onSubmit() {
  //   this.submitted = true;
  //
  //   // stop here if form is invalid
  //   if (this.registerForm.invalid) {
  //     return;
  //   }
  //   this.userInsert.username = this.registerForm.get('username').value;
  //   this.userInsert.password = this.registerForm.get('password').value;
  //   this.userInsert.type = this.registerForm.get('type').value;
  //   this.userService.insertUser(this.userInsert).subscribe(data => {
  //     alert(this.userInsert.username + ' successfull inserted');
  //     this.updateUserTable();
  //   });
  // }

}
