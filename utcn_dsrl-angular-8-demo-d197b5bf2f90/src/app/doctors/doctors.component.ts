import {Component, OnInit} from '@angular/core';
import {User} from '../model/User';
import {DoctorService} from '../services/doctor.service';
import {Router} from '@angular/router';
import {UserView} from '../model/UserView';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DoctorView} from "../model/DoctorView";
import {Doctor} from "../model/Doctor";

@Component({
  selector: 'app-users',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.css']
})
export class DoctorsComponent implements OnInit {

  doctors: DoctorView[] = [];
  doctorInsert: Doctor;
  displayedColumns: string[] = ['name', 'birthDate', 'adress','gender'];
  registerForm: FormGroup;
  submitted: boolean;
  disableButton = false;
  id : number;

  constructor(private router: Router, private doctorService: DoctorService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.submitted = false;
    this.updateDoctorTable();
  }

  updateDoctorTable() {
    this.registerForm = this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      birthdate: ['', [Validators.required]],
      adress: ['', Validators.required],
      gender: ['', Validators.required]
    });
    this.doctorInsert = new Doctor();
    this.doctorService.getDoctors()
      .subscribe(data => {
        console.log(data);
        this.doctors = data;
      });
  }

  get requestedForm() { return this.registerForm.controls; }

  deleteDoctorById(doctor: DoctorView){
    console.log(doctor);

    this.doctorService.deleteDoctorById(doctor.doctorId).pipe().subscribe(value=>{
      console.log("succes");
      this.doctors.splice(this.doctors.indexOf(doctor),1);
      this.doctorService.getDoctors();
    })
  }
}
