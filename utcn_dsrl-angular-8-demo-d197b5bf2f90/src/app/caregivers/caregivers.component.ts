import {Component, OnInit} from '@angular/core';
import {User} from '../model/User';
import {DoctorService} from '../services/doctor.service';
import {Router} from '@angular/router';
import {UserView} from '../model/UserView';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DoctorView} from "../model/DoctorView";
import {Doctor} from "../model/Doctor";
import {Caregiver} from "../model/Caregiver";
import {CaregiverView} from "../model/CaregiverView";
import {CaregiverService} from "../services/caregiver.service";

@Component({
  selector: 'app-users',
  templateUrl: './caregivers.component.html',
  styleUrls: ['./caregivers.component.css']
})
export class CaregiversComponent implements OnInit {

  caregivers: CaregiverView[] = [];
  caregiverInsert: Caregiver;
  displayedColumns: string[] = ['name', 'birthDate', 'adress', 'gender'];
  registerForm: FormGroup;
  submitted: boolean;

  constructor(private router: Router, private caregiverService: CaregiverService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.submitted = false;
    this.updateDoctorTable();
  }

  updateDoctorTable() {
    this.registerForm = this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      birthdate: ['', [Validators.required]],
      adress: ['', Validators.required],
      gender: ['', Validators.required]
    });
    this.caregiverInsert = new Caregiver();
    this.caregiverService.getCaregivers()
      .subscribe(data => {
        console.log(data);
        this.caregivers = data;
      });
  }

  get requestedForm() {
    return this.registerForm.controls;
  }

  deleteCaregiverById(caregiver: CaregiverView){
    console.log(caregiver);

    this.caregiverService.deleteCaregiverById(caregiver.id).pipe().subscribe(value=>{
      console.log("succes");
      this.caregivers.splice(this.caregivers.indexOf(caregiver),1);
      this.caregiverService.getCaregivers();
    })
  }

}
