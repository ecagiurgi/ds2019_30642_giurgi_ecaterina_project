import {Component, OnInit} from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {CaregiverService} from "../services/caregiver.service";

@Component({
  selector: 'app-user-activity',
  templateUrl: './user-activity.component.html',
  styleUrls: ['./user-activity.component.css']
})
export class UserActivityComponent implements OnInit {
  caregiverService: CaregiverService;
  webSocketEndPoint: string = 'http://localhost:8080/';
  stompClient: any;

  constructor(caregiverService: CaregiverService) {
    this.caregiverService = caregiverService;
  }


  ngOnInit() {
  }

  connect() {
    console.log("Initialize WebSocket Connection");
    setTimeout(() =>{

    },5000);
    console.log('Connected');
    //let ws = new SockJS(this.webSocketEndPoint + 'ws');
   // this.stompClient = Stomp.over(ws);
    this.caregiverService.getActivities().subscribe(value  => {
      let values : string[] = value;
      values.forEach( function(value) {
        console.log(value);
        }
      )
    })
  }

  disconnect() {
    console.log('Disconnected from WebSocket');
  }
}


