import {Component, OnInit} from '@angular/core';
import {UserService} from "../services/user.service";
import {Caregiver} from "../model/Caregiver";
import {CaregiverService} from "../services/caregiver.service";

@Component({
  selector: 'app-caregivers',
  templateUrl: './create-caregiver.component.html',
  styleUrls: ['./create-caregiver.component.css']
})
export class CreateCaregiverComponent implements OnInit {

  caregiver: Caregiver;

  constructor(private caregiverService: CaregiverService, private userService: UserService) {
    this.caregiver = {
      name: '',
      adress: '',
      birthDate: '',
      gender: '',
      user2DTO: {
        username: '',
        password: '',
        type: "CAREGIVER"
      }
    }
  }


  ngOnInit() {
  }

  onSubmit() {
    console.log(this.caregiver);
    this.caregiverService.insertCaregiver(this.caregiver).subscribe(value => {
      console.log(value);
      alert(this.caregiver.name + ' successfull inserted');
    })
  }

}
