import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Intake} from "../model/Intake";
import {IntakeService} from "../services/intake.service";

@Component({
  selector: 'app-users',
  templateUrl: './intakes.component.html',
  styleUrls: ['./intakes.component.css']
})
export class IntakesComponent implements OnInit {

  intakes: Intake[] = [];
  displayedColumns: string[] = ['medicationName' , 'dosage', 'startDate', 'endDate', 'status', 'patientName'];
  registerForm: FormGroup;
  submitted: boolean;
  disableButton = false;
  id : number;

  constructor(private router: Router, private intakeService: IntakeService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.submitted = false;
    this.updateIntakeTable();
  }

  updateIntakeTable() {
    this.registerForm = this.registerForm = this.formBuilder.group({
      medicationName: ['', Validators.required],
      dosage: ['', [Validators.required]],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      status: ['', Validators.required],
      patientName: ['', Validators.required]
    });
    this.intakeService.getIntakes()
      .subscribe(data => {
        console.log(data);
        this.intakes = data;
      });
  }

  get requestedForm() { return this.registerForm.controls; }

}
