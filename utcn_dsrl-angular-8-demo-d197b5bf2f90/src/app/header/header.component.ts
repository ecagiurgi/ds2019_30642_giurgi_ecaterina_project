import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
 public isButtonVisible: boolean;
  constructor(private router: Router, private userService: UserService) {
  }

  ngOnInit() {
    this.isButtonVisible = true;
  }

  getUsers() {
    this.router.navigate(['user']);
  }

  getDoctors() {
    this.router.navigate(['doctor']);
  }

  getCaregivers() {
    this.router.navigate(['caregiver']);
  }

  getPatients() {
    this.router.navigate(['patient']);
  }

  goHome() {
    this.router.navigate(['']);
  }

  goLogin() {
    this.router.navigate(['user/login']);
  }

  goLogout() {
    this.userService.logout();
    this.router.navigate(['']);
  }

  goAddDoctor() {
    this.router.navigate(['doctor/save']);
  }

  goAddPatient() {
    this.router.navigate(['patient/save']);
  }

  goAddCaregiver() {
    this.router.navigate(['caregiver/save']);
  }

  getIntakes() {
    this.router.navigate(['intake']);
  }

}
