import { Component, OnInit } from '@angular/core';
import {Doctor} from "../model/Doctor";
import {DoctorService} from "../services/doctor.service";
import {UserService} from "../services/user.service";
import {Patient} from "../model/Patient";
import {PatientService} from "../services/patient.service";

@Component({
  selector: 'app-patients',
  templateUrl: './create-patient.component.html',
  styleUrls: ['./create-patient.component.css']
})
export class CreatePatientComponent implements OnInit {

  patient: Patient;

  constructor(private patientService: PatientService, private userService: UserService) {
    this.patient = {
      name: '',
      adress: '',
      birthDate: '',
      gender: '',
      userDTO: {
        username: '',
        password: '',
        type: "PATIENT"
      },
      caregiverDTO: {
        id: 41,
        name: "Eca",
        birthDate: "1998-07-16",
        adress: "Cuj",
        gender: "FEMALE",
        user2DTO: {
          username: "eca7",
          password: "newpass",
          type: "CAREGIVER"
        }
      }
    }
  }


  ngOnInit() {
  }

  onSubmit() {
    console.log(this.patient);
    this.patientService.insertPatient(this.patient).subscribe(value => {
      console.log(value);
      alert(this.patient.name + ' successfull inserted');
    })
  }


}
